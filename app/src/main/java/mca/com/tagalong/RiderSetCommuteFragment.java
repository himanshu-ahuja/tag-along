package mca.com.tagalong;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

import mca.com.tagalong.mca.com.classes.SetCommuteRoute;

public class RiderSetCommuteFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    private final String TAG = "SET_SCHEDULE_FRAG";
    /* Firebase */
    protected DatabaseReference databaseReference;
    /* Shared Prefs */
    protected String SF_NAME = "docID";
    protected SharedPreferences sharedpreferences;
    protected String docID;

    /* Calendar */
    protected Calendar dateSelected = Calendar.getInstance();
    protected long maxDateLong;
    protected long minDateLong;
    private Button buttonElapsedSchedule;
    private Button buttonEditSchedule;
    private EditText editTextStartDate;
    private EditText editTextEndDate;
    private Spinner spinnerVehicleSelect;

    /* Activity */
    private String source;
    private String destination;
    private String startDate;
    private String endDate;
    private String startTime;
    private String selectedVehicle;
    private Boolean endDateInput = false;

    public RiderSetCommuteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        sharedpreferences = getActivity().getSharedPreferences(SF_NAME, Context.MODE_PRIVATE);
        docID = sharedpreferences.getString("documentID", null);
        Log.d("DOC_ID", "" + docID + "");

        View view = inflater.inflate(R.layout.fragment_rider_set_commute, container, false);
        Button buttonAddSchedule = view.findViewById(R.id.buttonAddSchedule);
        Button buttonSaveSchedule = view.findViewById(R.id.buttonSetSchedule);
        Button buttonViewSchedule = view.findViewById(R.id.buttonViewSchedule);

        buttonElapsedSchedule = view.findViewById(R.id.buttonElapsedSchedule);
        buttonEditSchedule = view.findViewById(R.id.buttonEditSchedule);
        editTextStartDate = view.findViewById(R.id.editTextStartDate);
        editTextEndDate = view.findViewById(R.id.editTextEndDate);
        spinnerVehicleSelect = view.findViewById(R.id.spinnerSelectVehicle);

        //Check Schedule Validity

        databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference riderRef = databaseReference.child("users").child("riders");
        final DatabaseReference riderScheduleRef = riderRef.child(docID).child("commute_details");

        riderScheduleRef.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    String endDate;
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        endDate = Objects.requireNonNull(postSnapshot.child("endDate").getValue()).toString();
                        Log.d(TAG, "" + endDate + "");
                        DateFormat dateFormat = new SimpleDateFormat("d/M/y", Locale.ENGLISH);
                        try {
                            Date endDateObject = dateFormat.parse(endDate);
                            Log.d(TAG, "" + endDateObject + "");

                            DateFormat dateFormatNow = new SimpleDateFormat("d/M/y", Locale.ENGLISH);
                            Date date = new Date();
                            System.out.println(dateFormatNow.format(date));

                            Log.d(TAG, "" + date + "");

                            if (date.after(endDateObject)) {

                                //Date Elapsed
                                String destinationPoint = Objects.requireNonNull(postSnapshot.child("destination").getValue()).toString();
                                String sourcePoint = Objects.requireNonNull(postSnapshot.child("source").getValue()).toString();
                                endDate = Objects.requireNonNull(postSnapshot.child("endDate").getValue()).toString();
                                String startDate = Objects.requireNonNull(postSnapshot.child("startDate").getValue()).toString();
                                String selectedVehicle = Objects.requireNonNull(postSnapshot.child("selectedVehicle").getValue()).toString();
                                String startTime = Objects.requireNonNull(postSnapshot.child("startTime").getValue()).toString();

                                HashMap<String, Object> scheduleMap = new HashMap<>();
                                scheduleMap.put("destination", destinationPoint);
                                scheduleMap.put("source", sourcePoint);
                                scheduleMap.put("startDate", startDate);
                                scheduleMap.put("endDate", endDate);
                                scheduleMap.put("selectedVehicle", selectedVehicle);
                                scheduleMap.put("startTime", startTime);
                                scheduleMap.put("status", "elapsed");

                                postSnapshot.getRef().updateChildren(scheduleMap);

                                View showView = getView().findViewById(R.id.linearLayoutScheduleElapsed);
                                showView.setVisibility(View.VISIBLE);

                                View hideView = getView().findViewById(R.id.linearLayoutNoScheduleFound);
                                hideView.setVisibility(View.INVISIBLE);
                            }
                            if (!date.after(endDateObject)) {
                                checkScheduleExists();
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Firebase Error Occurred", Toast.LENGTH_SHORT).show();
            }
        });

        /* Date */
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog dpd = new DatePickerDialog(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_DARK, this, year, month, day);
        DatePicker dp = dpd.getDatePicker();
        dp.setMinDate(c.getTimeInMillis());//get the current day
        //dp.setMinDate(System.currentTimeMillis() - 1000);// Alternate way to get the current day
        //Add 6 days with current date
        c.add(Calendar.DAY_OF_MONTH, 6);
        dp.setMaxDate(c.getTimeInMillis());

        editTextStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dpd.show();
            }
        });

        editTextEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                endDateInput = true;
                dpd.show();
            }
        });

        buttonAddSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View hideView = getView().findViewById(R.id.linearLayoutNoScheduleFound);
                hideView.setVisibility(View.INVISIBLE);

                View showView = getView().findViewById(R.id.linearLayoutSetSchedule);
                loadVehicleDetail();
                showView.setVisibility(View.VISIBLE);
            }
        });

        buttonSaveSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        //When time is selected
                        startDate = editTextStartDate.getText().toString().trim();
                        endDate = editTextEndDate.getText().toString().trim();
                        startTime = selectedHour + ":" + selectedMinute;

                        Boolean result = verifyFields();
                        if (result) {
                            databaseReference = FirebaseDatabase.getInstance().getReference();
                            DatabaseReference riderRef = databaseReference.child("users").child("riders");
                            final SetCommuteRoute setCommuteRoute = new SetCommuteRoute(startDate, endDate, source, destination, startTime, selectedVehicle, "ongoing");
                            final DatabaseReference subRootRef = riderRef.child(docID).child("commute_details");
                            subRootRef.addValueEventListener(new ValueEventListener() {
                                @SuppressLint("ResourceAsColor")
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.getChildrenCount() > 0) {
                                        //Update Existing Schedule
                                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                            HashMap<String, Object> scheduleMap = new HashMap<>();
                                            scheduleMap.put("destination", destination);
                                            scheduleMap.put("endDate", endDate);
                                            scheduleMap.put("selectedVehicle", selectedVehicle);
                                            scheduleMap.put("source", source);
                                            scheduleMap.put("startDate", startDate);
                                            scheduleMap.put("startTime", startTime);
                                            scheduleMap.put("status", "ongoing");
                                            postSnapshot.getRef().updateChildren(scheduleMap);
                                            Toast.makeText(getActivity(), "Schedule Updated Success", Toast.LENGTH_SHORT).show();
                                            String contactNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                            Log.d(TAG, "" + contactNumber + "");
                                            Intent intent = new Intent(getActivity(), RiderMainActivity.class);
                                            intent.putExtra("contactNumber", contactNumber);
                                            startActivity(new Intent(getActivity(), RiderMainActivity.class));
                                        }
                                    }
                                    if (dataSnapshot.getChildrenCount() <= 0) {
                                        //Set New Schedule
                                        final DatabaseReference commuteRef = subRootRef.push();
                                        commuteRef.setValue(setCommuteRoute)
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Toast.makeText(getActivity(), "Schedule Set Success!", Toast.LENGTH_SHORT).show();
                                                        String contactNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                                        Log.d(TAG, "" + contactNumber + "");
                                                        Intent intent = new Intent(getActivity(), RiderMainActivity.class);
                                                        intent.putExtra("contactNumber", contactNumber);
                                                        startActivity(new Intent(getActivity(), RiderMainActivity.class));
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.d("ERR: ", "" + e.getMessage() + "");
                                                        Toast.makeText(getActivity(), "Could not set schedule. Please try again later", Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    Toast.makeText(getActivity(), "Firebase Error. Try Again Later", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle(getString(R.string.start_time));
                mTimePicker.show();
            }
        });

        buttonViewSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment;
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragment = new ViewCommuteRouteFragment();
                fragmentTransaction.replace(R.id.frameLayoutContent, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        buttonEditSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View hideView = getView().findViewById(R.id.linearLayoutScheduleFound);
                hideView.setVisibility(View.INVISIBLE);

                View showView = getView().findViewById(R.id.linearLayoutSetSchedule);
                loadVehicleDetail();
                showView.setVisibility(View.VISIBLE);
            }
        });

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("IND")
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                .build();

        PlaceAutocompleteFragment sourceAutoCompleteFragment = (PlaceAutocompleteFragment) getChildFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        sourceAutoCompleteFragment.setFilter(typeFilter);
        sourceAutoCompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                source = place.getLatLng().toString();
                // TODO: Get info about the selected place.
                Log.i("SCHEDULE", "Source" + source);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    Toast.makeText(getContext(), place.getName(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("MAPS", "An error occurred: " + status);
            }
        });

        PlaceAutocompleteFragment destinationAutoCompleteFragment = (PlaceAutocompleteFragment) getChildFragmentManager().findFragmentById(R.id.destination_autocomplete_fragment);
        destinationAutoCompleteFragment.setFilter(typeFilter);
        destinationAutoCompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place destinationPlace) {
                destination = destinationPlace.getLatLng().toString();
                // TODO: Get info about the selected place.
                Log.i("SCHEDULE", "Destination" + destination);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    Toast.makeText(getContext(), destinationPlace.getName(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("MAPS", "An error occurred: " + status);
            }
        });

        buttonElapsedSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
                builder.setMessage("Do you wish to delete existing schedule?")
                        .setTitle("Delete Schedule");
                builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                        DatabaseReference riderRef = databaseReference.child("users").child("riders");
                        final DatabaseReference commuteRef = riderRef.child(docID).child("commute_details");
                        commuteRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.getChildrenCount() > 0) {
                                    dataSnapshot.getRef().setValue(null);
                                    View showView = getView().findViewById(R.id.linearLayoutSetSchedule);
                                    loadVehicleDetail();
                                    showView.setVisibility(View.VISIBLE);
                                    View hideView = getView().findViewById(R.id.linearLayoutScheduleElapsed);
                                    hideView.setVisibility(View.INVISIBLE);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getActivity(), "Firebase Error. Please try again later.", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                android.support.v7.app.AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        return view;

    }


    //Check if Schedule Exists
    protected void checkScheduleExists() {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference riderRef = databaseReference.child("users").child("riders");
        DatabaseReference riderScheduleRef = riderRef.child(docID).child("commute_details");
        riderScheduleRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "" + dataSnapshot.getChildrenCount() + "");
                if (dataSnapshot.getChildrenCount() > 0) {
                    //Schedule Exists Check Status Now
                    String status;
                    for (DataSnapshot postSnapsot : dataSnapshot.getChildren()) {
                        status = Objects.requireNonNull(postSnapsot.child("status").getValue().toString());
                        if (status.equals("ongoing")) {
                            View hideView = Objects.requireNonNull(getView()).findViewById(R.id.linearLayoutNoScheduleFound);
                            hideView.setVisibility(View.INVISIBLE);
                            View hideView1 = Objects.requireNonNull(getView()).findViewById(R.id.linearLayoutScheduleElapsed);
                            hideView1.setVisibility(View.INVISIBLE);

                            View showView = getView().findViewById(R.id.linearLayoutScheduleFound);
                            showView.setVisibility(View.VISIBLE);
                        }
                        if (status.equals("elapsed")) {
                            View hideView = Objects.requireNonNull(getView()).findViewById(R.id.linearLayoutNoScheduleFound);
                            hideView.setVisibility(View.INVISIBLE);
                            View showView = getView().findViewById(R.id.linearLayoutScheduleFound);
                            showView.setVisibility(View.INVISIBLE);

                            View hideView1 = Objects.requireNonNull(getView()).findViewById(R.id.linearLayoutScheduleElapsed);
                            hideView1.setVisibility(View.VISIBLE);
                        }

                    }

                }
                if (dataSnapshot.getChildrenCount() <= 0) {
                    //No schedule found
                    View showView = getView().findViewById(R.id.linearLayoutSetSchedule);
                    showView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

    }

    public Boolean verifyFields() {

        if (TextUtils.isEmpty(source)) {
            Toast.makeText(getActivity(), "Select Source", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(destination)) {
            Toast.makeText(getActivity(), "Select Source", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(startDate)) {
            Toast.makeText(getActivity(), "Select Start Date", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(endDate)) {
            endDate = "-";
            return true;
        }
        return true;
    }

    protected void loadVehicleDetail() {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference riderRef = databaseReference.child("users").child("riders");
        DatabaseReference riderScheduleRef = riderRef.child(docID).child("vehicle_details");
        riderScheduleRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        selectedVehicle = Objects.requireNonNull(postSnapshot.child("vehicleRegistrationNumber").getValue()).toString();
                        ArrayList<String> vehicles = new ArrayList<String>();
                        vehicles.add(selectedVehicle);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, vehicles);
                        spinnerVehicleSelect.setAdapter(adapter);
                    }
                }
                if (dataSnapshot.getChildrenCount() <= 0) {
                    //No schedule found
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        int yearSelected = year;
        int monthSelected = month + 1;
        int daySelected = dayOfMonth;
        if (!endDateInput) {
            editTextStartDate.setText("" + dayOfMonth + "/" + monthSelected + "/" + yearSelected + "");
        }
        if (endDateInput) {
            editTextEndDate.setText("" + dayOfMonth + "/" + monthSelected + "/" + yearSelected + "");

        }
    }
}
