package mca.com.tagalong;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class profileView extends Fragment {

    protected final String TAG = "PROFILE_FRAG";

    protected String SF_NAME = "docID";
    protected SharedPreferences sharedpreferences;
    protected String docID;

    protected FirebaseAuth firebaseAuth;

    ImageView imageView;
    TextView textViewName;
    TextView textViewContact;
    TextView textViewTripsCompleted;
    TextView textViewHomeAddress;
    TextView textViewDestinationAddress;
    TextView textViewVehicleDetails;
    Button buttonEditHome;
    Button buttonDeleteHome;
    Button buttonEditDestination;
    Button buttonDeleteDestination;
    Button buttonEditVehicle;

    protected LatLng sourceLatLng, destinationLatLng;
    protected String sourceLat, sourceLang;
    protected String destinationLat, destinationLang;

    public profileView() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_view, container, false);

        imageView = view.findViewById(R.id.imageViewUser);
        textViewName = view.findViewById(R.id.textViewName);
        textViewContact = view.findViewById(R.id.textViewContact);
        textViewTripsCompleted = view.findViewById(R.id.textViewTrips);
        textViewHomeAddress = view.findViewById(R.id.textViewHome);
        textViewDestinationAddress = view.findViewById(R.id.textViewDestination);
        textViewVehicleDetails = view.findViewById(R.id.textViewVehicle);
        buttonEditHome = view.findViewById(R.id.buttonEdit);
        buttonDeleteHome = view.findViewById(R.id.buttonDelete);
        buttonEditDestination = view.findViewById(R.id.buttonDestinationEdit);
        buttonDeleteDestination = view.findViewById(R.id.buttonDestinationDelete);
        buttonEditVehicle = view.findViewById(R.id.buttonVehicleEdit);

        firebaseAuth = FirebaseAuth.getInstance();

        sharedpreferences = getActivity().getSharedPreferences(SF_NAME,
                Context.MODE_PRIVATE);

        docID = sharedpreferences.getString("documentID", null);
        Log.d("" + TAG + "DOC_ID", "" + docID + "");


        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref = databaseReference.child("users");
        DatabaseReference riderRef = ref.child("riders").child(docID);

        riderRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    Log.d("CHILDREN_COUNT", "" + String.valueOf(dataSnapshot.getChildrenCount()) + "");
                    String firstName = dataSnapshot.child("firstName").getValue(String.class);
                    String lastName = dataSnapshot.child("lastName").getValue(String.class);
                    String contactNumber = dataSnapshot.child("contactNumber").getValue(String.class);
                    String gender = dataSnapshot.child("gender").getValue(String.class);
                    String riderName = firstName + " " + lastName;
                    Log.d("RIDER_NAME", " " + firstName + " ");
                    Log.d("MOBILE", " " + contactNumber + " ");
                    textViewName.setText(getString(R.string.blank_string, riderName));
                    textViewContact.setText(getString(R.string.blank_string, contactNumber));
                    if (gender.equals("Male")) {
                        imageView.setImageResource(R.drawable.boy);
                    }
                    if (gender.equals("Female")) {
                        imageView.setImageResource(R.drawable.woman);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Firebase Error. Try Again Later!", Toast.LENGTH_SHORT).show();
            }
        });

        DatabaseReference commuteRef = ref.child("riders").child(docID).child("commute_details");
        commuteRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot commuteSnapshot : dataSnapshot.getChildren()) {
                        Log.d("childCount", String.valueOf(commuteSnapshot.getChildrenCount()));
                        String sourcePoint = commuteSnapshot.child("source").getValue(String.class);
                        String scheduleTime = commuteSnapshot.child("startTime").getValue(String.class);
                        String startDate = commuteSnapshot.child("startDate").getValue(String.class);
                        String endDate = commuteSnapshot.child("endDate").getValue(String.class);

                        List<String> sourceLangList = Arrays.asList(Objects.requireNonNull(sourcePoint).split(","));
                        sourceLat = sourcePoint.substring(10, 19);
                        sourceLang = sourceLangList.get(1).substring(0, 8);
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(getActivity(), Locale.getDefault());
                        try {
                            String sPlace;
                            addresses = geocoder.getFromLocation(Double.parseDouble(sourceLat), Double.parseDouble(sourceLang), 1);
                            String address = addresses.get(0).getAddressLine(0);
                            String city = addresses.get(0).getAddressLine(1);
                            String country = addresses.get(0).getAddressLine(2);

                            textViewHomeAddress.setText("");
                            textViewHomeAddress.setText(getString(R.string.blank_string, address));

                            String[] splitAddress = address.split(",");
                            sPlace = splitAddress[0] + "\n";
                            if (city != null && !city.isEmpty()) {
                                String[] splitCity = city.split(",");
                                sPlace += splitCity[0];
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        String destinationPoint = commuteSnapshot.child("destination").getValue(String.class);
                        List<String> destinationLangList = Arrays.asList(destinationPoint.split(","));
                        destinationLat = destinationPoint.substring(10, 19);
                        destinationLang = destinationLangList.get(1).substring(0, 8);

                        Geocoder geocoder1;
                        List<Address> addresses1;
                        geocoder = new Geocoder(getActivity(), Locale.getDefault());
                        try {
                            String sPlace1;
                            addresses1 = geocoder.getFromLocation(Double.parseDouble(destinationLat), Double.parseDouble(destinationLang), 1);
                            String address1 = addresses1.get(0).getAddressLine(0);
                            String city1 = addresses1.get(0).getAddressLine(1);
                            String country1 = addresses1.get(0).getAddressLine(2);

                            textViewDestinationAddress.setText("");
                            textViewDestinationAddress.setText(getString(R.string.blank_string, address1));

                            String[] splitAddress1 = address1.split(",");
                            sPlace1 = splitAddress1[0] + "\n";
                            if (city1 != null && !city1.isEmpty()) {
                                String[] splitCity = city1.split(",");
                                sPlace1 += splitCity[0];
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (dataSnapshot.getChildrenCount() < 0) {
                    Toast.makeText(getActivity(), "Schedule is not set", Toast.LENGTH_SHORT).show();
                    textViewHomeAddress.setText("Schedule is not set");
                    textViewDestinationAddress.setText("Schedule is not set");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Firebase Error while fetching Schedule. Check Internet Connectivity!", Toast.LENGTH_SHORT).show();
            }
        });

        DatabaseReference vehicleRef = riderRef.child("vehicle_details");
        vehicleRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 1) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        String registrationNumber = Objects.requireNonNull(postSnapshot.child("vehicleRegistrationNumber").getValue()).toString();
                        String DLNumber = Objects.requireNonNull(postSnapshot.child("vehicleDLNumber").getValue()).toString();
                        String vehicleDesc = Objects.requireNonNull(postSnapshot.child("vehicleDesc").getValue()).toString();
                        Boolean pillionHelmet = Objects.requireNonNull((Boolean) postSnapshot.child("pillionHelmet").getValue());
                        String vehicleClassLocal = Objects.requireNonNull(postSnapshot.child("vehicleClass").getValue()).toString();

                        String vehicleDetails = vehicleClassLocal + " " + vehicleDesc + " " + " Reg. No : "  +registrationNumber + " DL : " + DLNumber;
                        textViewVehicleDetails.setText(getString(R.string.blank_string, vehicleDetails));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Firebase Error while fetching Vehicle Details. Check Internet Connectivity!", Toast.LENGTH_SHORT).show();
            }
        });

        buttonEditHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment;
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragment = new RiderSetCommuteFragment();
                fragmentTransaction.replace(R.id.frameLayoutContent, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        buttonDeleteHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Do you wish to delete existing schedule?")
                        .setTitle("Delete Schedule");
                builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                        DatabaseReference riderRef = databaseReference.child("users").child("riders");
                        final DatabaseReference commuteRef = riderRef.child(docID).child("commute_details");
                        commuteRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.getChildrenCount() > 0) {
                                    dataSnapshot.getRef().setValue(null);
                                  /*  String contactNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                    Log.d(TAG, ""+contactNumber+"");
                                    Intent intent = new Intent(getActivity(), RiderMainActivity.class);
                                    intent.putExtra("contactNumber", contactNumber);
                                    startActivity(new Intent(getActivity(), RiderMainActivity.class));*/
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getActivity(), "Error While Deleting Schedule. Please try again later.", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        buttonDeleteDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Do you wish to delete existing schedule?")
                        .setTitle("Delete Schedule");
                builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                        DatabaseReference riderRef = databaseReference.child("users").child("riders");
                        final DatabaseReference commuteRef = riderRef.child(docID).child("commute_details");
                        commuteRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.getChildrenCount() > 0) {
                                    dataSnapshot.getRef().setValue(null);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getActivity(), "Firebase Error. Please try again later.", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        buttonEditDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment;
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragment = new RiderSetCommuteFragment();
                fragmentTransaction.replace(R.id.frameLayoutContent, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        buttonEditVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment;
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragment = new AddVehicleFragment();
                fragmentTransaction.replace(R.id.frameLayoutContent, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

}
