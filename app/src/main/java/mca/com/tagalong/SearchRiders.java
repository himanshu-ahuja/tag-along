package mca.com.tagalong;


import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import mca.com.tagalong.mca.com.classes.GMapV2Direction;

public class SearchRiders extends Fragment {

    public SearchRiders() {
    }

    private final String TAG = "SEARCH_RIDERS";
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    protected String SF_NAME = "docID";
    protected SharedPreferences sharedpreferences;
    protected String docID;

    private String source;
    private String destination;
    private String date;
    private String time;
    private Double sourceLat;
    private Double sourceLng;
    private Double destinationLat;
    private Double destinationLng;

    private TextView textViewPassengerSourceAddress;
    private TextView textViewPassengerDestinationAddress;
    private TextView textViewPassengerDate;
    private TextView textViewPassengerTime;
    private TextView textViewDistance;
    private TextView textViewJourneyTime;


    private int radius = 1;
    private Boolean riderFound = false;
    private String riderFoundID;
    GeoQuery geoQuery;

    private LatLng pickupLocation;

    private CardView cardView1;
    private ImageView imageView1;
    private TextView textViewRiderName1;
    private TextView textViewContactNumber1;
    private TextView textViewGender1;
    private Button buttonRider1;
    private Button buttonEdit;

    String userID;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_riders, container, false);

        Bundle bundle = getArguments();

        sharedpreferences = getActivity().getSharedPreferences(SF_NAME,
                Context.MODE_PRIVATE);
        docID = sharedpreferences.getString("documentID", null);
        Log.d("" + TAG + "DOC_ID", "" + docID + "");

        cardView1 = view.findViewById(R.id.cardView1);
        imageView1 = view.findViewById(R.id.imageView1);
        textViewRiderName1 = view.findViewById(R.id.textViewRiderName1);
        textViewGender1 = view.findViewById(R.id.textViewRiderGender1);
        ;
        textViewContactNumber1 = view.findViewById(R.id.textViewRiderContactNumber1);
        buttonRider1 = view.findViewById(R.id.buttonRider1);

        buttonEdit = view.findViewById(R.id.buttonEditSearchRider);

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(),PassengerMain.class));
                getActivity().finish();
                            }
        });

       /* source = bundle.getString("source");
        destination = bundle.getString("destination");*/
        date = bundle.getString("date");
        time = bundle.getString("time");

      /*  sourceLat = bundle.getDouble("sourceLat");
        sourceLng = bundle.getDouble("sourceLng");
        destinationLat = bundle.getDouble("destinationLat");
        destinationLng = bundle.getDouble("destinationLng");*/

        sourceLat = bundle.getDouble("srcLat");
        sourceLng = bundle.getDouble("srcLng");
        destinationLat = bundle.getDouble("destLat");
        destinationLng = bundle.getDouble("destLng");

        textViewPassengerSourceAddress = view.findViewById(R.id.textViewPassengerSourceAddress);
        textViewPassengerDestinationAddress = view.findViewById(R.id.textViewPassengerDestinationAddress);
        textViewPassengerDate = view.findViewById(R.id.textViewPassengerDate);
        textViewPassengerTime = view.findViewById(R.id.textViewPassengerTime);
        textViewJourneyTime = view.findViewById(R.id.textViewJourneyTime);
        textViewDistance = view.findViewById(R.id.textViewDistance);

        LatLng sourceLatLng = new LatLng(sourceLat, sourceLng);
        LatLng dropLatLng = new LatLng(destinationLat, destinationLng);

        Location locationA = new Location("");
        locationA.setLatitude(sourceLatLng.latitude);
        locationA.setLongitude(sourceLatLng.longitude);

        Location locationB = new Location("");
        locationB.setLatitude(dropLatLng.latitude);
        locationB.setLongitude(dropLatLng.longitude);

        float d = locationA.distanceTo(locationB) / 1000;
        Log.d(TAG, "" + d + "");

        Log.d("Source_Lat", "" + sourceLatLng.latitude + "");
        Log.d("Source_Lng", "" + sourceLatLng.longitude + "");
        Log.d("Dest_Lat", "" + dropLatLng.latitude + "");
        Log.d("Dest_Lng", "" + dropLatLng.longitude + "");

        float results[] = new float[5];
        Location.distanceBetween(sourceLatLng.latitude, sourceLatLng.longitude, dropLatLng.latitude, dropLatLng.longitude, results);
        //Getting URL to the Google Directions API
        LatLng origin = sourceLatLng;
        LatLng dest = dropLatLng;

        String url = getDirectionsUrl(origin, dest);
        SearchRiders.DownloadTask downloadTask = new DownloadTask();
        // Start downloading json data from Google Directions API
        downloadTask.execute(url);

        SearchRiders.getDuration getDuration = new getDuration();
        String[] distanceAddress = new String[5];
        distanceAddress[0] = sourceLat.toString();
        distanceAddress[1] = sourceLng.toString();
        distanceAddress[2] = destinationLat.toString();
        distanceAddress[3] = destinationLng.toString();
        distanceAddress[4] = "driving";
        getDuration.execute(distanceAddress);

        String sourceAddress = getAddress(sourceLat, sourceLng);
        if (sourceAddress == "") {
            textViewPassengerSourceAddress.setText("Error in Fetching..");
        }
        if (!(sourceAddress == "")) {
            textViewPassengerSourceAddress.setText("" + sourceAddress + "");
        }

        String destAddress = getAddress(destinationLat, destinationLng);
        if (destAddress == "0") {
            textViewPassengerDestinationAddress.setText("Error in Fetching..");
        }
        if (!(sourceAddress == "0")) {
            textViewPassengerDestinationAddress.setText("" + destAddress + "");
        }

        textViewPassengerDate.setText(" Date : " + date + "");
        textViewPassengerTime.setText(" Time : " + time + "");

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference usersRef = databaseReference.child("users");
        final DatabaseReference riderRef = usersRef.child("riders");


        /* Load Riders */

        //userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        userID = docID;
        Log.d("USER_ID", "" + userID + "");
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("customerRequest");
        GeoFire geoFire = new GeoFire(ref);
        geoFire.setLocation(userID, new GeoLocation(sourceLat, sourceLng));

        pickupLocation = new LatLng(sourceLat, sourceLng);


        buttonRider1.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                DatabaseReference driverRef = FirebaseDatabase.getInstance().getReference().child("users").child("riders").child(riderFoundID).child("customerRequest");
                //String customerId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                String customerId = docID;

                HashMap map = new HashMap();
                map.put("customerRideID", customerId);
                map.put("destination", destination);
                map.put("destinationLat", destinationLat);
                map.put("destinationLng", destinationLng);
                driverRef.updateChildren(map);
                Toast.makeText(getActivity(), "Request has been sent", Toast.LENGTH_SHORT).show();

                /* Notification */

                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(getActivity())
                                .setSmallIcon(R.drawable.ic_sports_bike)
                                .setContentTitle("Awaiting Confirmation")
                                .setContentText("Request Sent to " + textViewRiderName1.getText().toString() + "");

                Intent notificationIntent = new Intent(getActivity(), PassengerMain.class);
                PendingIntent contentIntent = PendingIntent.getActivity(getActivity(), 0, notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(contentIntent);

                // Add as notification
                NotificationManager manager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(0, builder.build());


                buttonRider1.setText("SENT");
                buttonRider1.setClickable(false);

            }
        });

        getClosestRider();

        return view;
    }

    private void getClosestRider() {
        DatabaseReference riderLocation = FirebaseDatabase.getInstance().getReference().child("ridersAvailable");

        GeoFire geoFire = new GeoFire(riderLocation);

        geoQuery = geoFire.queryAtLocation(new GeoLocation(pickupLocation.latitude, pickupLocation.longitude), radius);
        geoQuery.removeAllListeners();

        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(final String key, GeoLocation location) {

                Log.d("RIDER_FOUND", "" + riderFound + "");

                if (!riderFound) {
                    Log.d("GFIRE_KEY", "" + key + "");

                    DatabaseReference passengerReference = FirebaseDatabase.getInstance().getReference().child("users").child("riders").child(key);

                    /*DatabaseReference passengerReference = FirebaseDatabase.getInstance().getReference().child("users").child("riders").child(key).child("commute_details");*/

                    passengerReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @TargetApi(Build.VERSION_CODES.O)
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {


                                /*Map<String, Object> riderMap = (Map<String, Object>) dataSnapshot.getValue();
                                String endDate = riderMap.get(0).toString();
                                String startDate = riderMap.get("startDate").toString();
                                String startTime = riderMap.get("startTime").toString();*/
                                String endDate = null;
                                String startDate = null;
                                String startTime = null;
                                riderFoundID = key;

                                if (riderFound) {
                                    return;
                                }

                                cardView1.setVisibility(View.VISIBLE);
                                riderFound = true;
                                Log.d("RIDER_FOUND_ID", "" + riderFoundID + "");
                                buttonRider1.setVisibility(View.VISIBLE);
                                getRiderInfo();

                                if (!riderFound) {
                                    Toast.makeText(getActivity(), "No Rider Available. Searching...", Toast.LENGTH_SHORT).show();
                                }

/*
                                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                    endDate = postSnapshot.child("endDate").getValue().toString();
                                    startDate = postSnapshot.child("startDate").getValue().toString();
                                    startTime = postSnapshot.child("startTime").getValue().toString();

                                    Log.d("DATE", "" + endDate + "");
                                    Log.d("DATE", "" + startDate + "");
                                    Log.d("TIME", "" + startTime + "");

                                    riderFoundID = key;
                                }

                                DateFormat dateFormat = new SimpleDateFormat("d/M/y", Locale.ENGLISH);
                                DateFormat format = new SimpleDateFormat("HH:mm");

                                Date endDateObject = null;
                                Date startDateObject = null;

                                Date date = null;
                                Date dateTime = null;

                                long timeElapsed = 0;
                                long timeElapsedHr = 0;
                                long timeElapsedMin = 0;

                                try {
                                    endDateObject = dateFormat.parse(endDate);
                                    startDateObject = dateFormat.parse(startDate);
                                    date = new Date();
                                    dateTime = new Date();
                                    String todayDate = dateFormat.format(date);
                                    String nowTime = format.format(dateTime);

                                    //Time
                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
                                    Date d1 = simpleDateFormat.parse(startTime);
                                    Date d2 = simpleDateFormat.parse(nowTime);

                                    timeElapsed = d2.getTime() - d1.getTime();
                                    timeElapsedMin = timeElapsed / 60000;
                                    timeElapsedHr = timeElapsedMin / 60;

                                    Log.d(TAG, "Time Elapsed : " + timeElapsedHr+ " Hrs.");

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                if (riderFound) {
                                    return;
                                }

                                if (!(date.after(endDateObject))) {

                                    if (timeElapsedHr > .30) {

                                        cardView1.setVisibility(View.VISIBLE);
                                        riderFound = true;
                                        Log.d("RIDER_FOUND_ID", "" + riderFoundID + "");
                                        buttonRider1.setVisibility(View.VISIBLE);
                                        getRiderInfo();
                                    }

                                    if (timeElapsedHr <= .30) {
                                        Toast.makeText(getActivity(), "No Rider Available. Try Again Later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                                if ((date.after(endDateObject) && date.after(startDateObject))) {
                                    Toast.makeText(getActivity(), "No Rider Available. Try Again Later", Toast.LENGTH_SHORT).show();

                                }*/
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onKeyExited(String key) {

            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {
                if (!riderFound) {
                    radius++;
                    getClosestRider();
                }
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });

    }


    /*-------------------------------------------- getRiderInfo -----
    |  Function(s) getRiderInfo
    |
    |  Purpose:  Get all the user information that we can get from the user's database.
    |
    |  Note: --: --
    |
    *-------------------------------------------------------------------*/
    private void getRiderInfo() {
        cardView1.setVisibility(View.VISIBLE);
        Log.d("RIDER_ID", "" + riderFoundID + "");

        DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("users").child("riders").child(riderFoundID);
        mCustomerDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {

                    String firstName = null;
                    String lastName = null;
                    String contactNumber = null;
                    String gender = null;

                    if (dataSnapshot.child("firstName") != null) {
                        firstName = dataSnapshot.child("firstName").getValue().toString();
                    }
                    if (dataSnapshot.child("lastName") != null) {
                        lastName = dataSnapshot.child("lastName").getValue().toString();
                    }
                    if (dataSnapshot.child("contactNumber") != null) {
                        contactNumber = dataSnapshot.child("contactNumber").getValue().toString();
                    }
                    if (dataSnapshot.child("gender").getValue() != null) {
                        gender = dataSnapshot.child("gender").getValue().toString();
                    }
                    if (gender.equals("Female")) {
                        imageView1.setImageResource(R.drawable.woman);
                    }
                    String riderName = firstName + " " + lastName;
                    textViewRiderName1.setText("" + riderName + "");
                    textViewContactNumber1.setText("" + contactNumber + "");
                    textViewGender1.setText("" + gender + "");

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


    private class getDuration extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            Location sourceLocation = new Location("google");
            Location destinationLocation = new Location("google");

            sourceLocation.setLatitude(Double.parseDouble(strings[0]));
            sourceLocation.setLongitude(Double.parseDouble(strings[1]));
            destinationLocation.setLatitude(Double.parseDouble(strings[2]));
            destinationLocation.setLongitude(Double.parseDouble(strings[3]));

            Log.d(TAG, "SrcLat:" + sourceLocation.getLatitude() + "");
            Log.d(TAG, "SrcLng:" + sourceLocation.getLongitude() + "");
            Log.d(TAG, "DestLat:" + destinationLocation.getLatitude() + "");
            Log.d(TAG, "DestLng:" + destinationLocation.getLongitude() + "");

            Document document = new GMapV2Direction().getDocument(sourceLocation, destinationLocation, "driving");
            int distance = new GMapV2Direction().getDistanceValue(document);
            Log.d(TAG, "D ::: " + distance + "");
            return String.valueOf(distance / 1000);
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(TAG, "" + result + " Kms.");
            if (result == "0") {
                result = "GeoFireError";
                textViewDistance.setText(getString(R.string.blank_string, result));
            }
            if ((result != "0") || (result != "GeoFireError")) {
                String dist = "Total Distance " + result + " Kms";
                textViewDistance.setText(getString(R.string.blank_string, dist));
            }

        }

    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);

            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

        }
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Checks, whether start and end locations are captured
    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
    }

    public String getAddress(double lat, double lng) {
        String add = "";
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            add = obj.getAddressLine(0);
           /* add = add + "\n" + obj.getCountryName();
            add = add + "\n" + obj.getCountryCode();
            add = add + "\n" + obj.getAdminArea();
            add = add + "\n" + obj.getPostalCode();
            add = add + "\n" + obj.getSubAdminArea();
            add = add + "\n" + obj.getLocality();
            add = add + "\n" + obj.getSubThoroughfare();*/


        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return add;
    }

}
