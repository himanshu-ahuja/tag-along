package mca.com.tagalong;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;

import com.felipecsl.gifimageview.library.GifImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;


public class Splash extends AppCompatActivity {

    private static final String TAG = "SPLASH_ACTIVITY";
    private GifImageView gif;
    private String mobileNumber;
    private String SF_NAME = "docID";
    private SharedPreferences sharedpreferences;
    private String userID;
    private String userType;

    /* Firebase */
    private DatabaseReference mDatabaseReference;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    // Duration of wait
    private final int SPLASH_DISPLAY_LENGTH = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

       /* gif = findViewById(R.id.gif);
        try {
            InputStream inputStream = getAssets().open("giphy.gif");
            byte[] bytes = IOUtils.toByteArray(inputStream);
            gif.setBytes(bytes);
            gif.startAnimation();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        ConstraintLayout constraintLayout= findViewById(R.id.layout);
        AnimationDrawable animationDrawable=(AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(1500);
        animationDrawable.setExitFadeDuration(2000);
        animationDrawable.start();


        mAuth = FirebaseAuth.getInstance();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("FireAuth", "onAuthStateChanged:signed_in:" + user.getUid());
                    userID = mAuth.getCurrentUser().getUid();
                    Log.d("UID", userID);

                    sharedpreferences = getApplicationContext().getSharedPreferences(SF_NAME, Context.MODE_PRIVATE);
                    final String docID = sharedpreferences.getString("documentID", null);
                    Log.d("DOC_ID", "" + docID + "");

                    mobileNumber = user.getPhoneNumber().substring(3);
                    Log.d("NUMBER", mobileNumber);

                            DatabaseReference userRef = mDatabaseReference.child("users");
                            //check if user=>passenger
                            DatabaseReference passengersRef = userRef.child("passengers");
                            Query query = passengersRef.orderByChild("contactNumber").equalTo(mobileNumber);
                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    Log.d("P_COUNT", String.valueOf(dataSnapshot.getChildrenCount()));
                                    if (dataSnapshot.getChildrenCount() > 0) {
                                        Intent intent = new Intent(getApplicationContext(), PassengerMainActivity.class);
                                        intent.putExtra("contactNumber", mobileNumber);
                                        startActivity(intent);
                                        finish();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    Log.d("CLASS", "Firebase Error");
                                }
                            });
                            //check if user=>rider
                            DatabaseReference riderRef = userRef.child("riders");
                            Query query1 = riderRef.orderByChild("contactNumber").equalTo(mobileNumber);
                            query1.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    Log.d("R_COUNT", String.valueOf(dataSnapshot.getChildrenCount()));
                                    if (dataSnapshot.getChildrenCount() > 0) {
                                        Intent intent = new Intent(getApplicationContext(), RiderMainActivity.class);
                                        intent.putExtra("contactNumber", mobileNumber);
                                        startActivity(intent);
                                        finish();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    Log.d("CLASS", "Firebase Error");
                                }
                            });

                } else {
                    // User is signed out
                    Log.d("FireAuth", "onAuthStateChanged:signed_out");
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
    }
    @Override
    public void onStart() {
        super.onStart();
        //add listener
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

}
