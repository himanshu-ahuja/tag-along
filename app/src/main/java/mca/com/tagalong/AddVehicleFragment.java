package mca.com.tagalong;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

import mca.com.tagalong.mca.com.classes.AddVehicleDetails;
import mca.com.tagalong.mca.com.verification.DrivingLicenseList;

public class AddVehicleFragment extends Fragment {

    private static final String TAG = "VEHICLE";
    protected FirebaseAuth firebaseAuth;
    protected DatabaseReference databaseReference;
    protected String SF_NAME = "docID";
    protected SharedPreferences sharedpreferences;
    protected String docID;

    Spinner spinnerVehicleClass;
    Button buttonVehicleRegistrationSave;
    Button buttonVehicleRegistrationUpdate;
    ImageView imageViewVehicleRegister;
    EditText editTextVehicleDescription;
    EditText editTextRegistrationNumber;
    EditText editTextDrivingLicense;
    CheckBox checkboxHelmetPrompt;

    DrivingLicenseList drivingLicenseList = new DrivingLicenseList();
    private String drivingLicenseNumber;
    private String vehicleClass;

    public AddVehicleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_add_vehicle, container, false);

        sharedpreferences = getActivity().getSharedPreferences(SF_NAME, Context.MODE_PRIVATE);
        docID = sharedpreferences.getString("documentID", null);
        Log.d(""+TAG+"DOC_ID", "======" + docID + "======");

        databaseReference = FirebaseDatabase.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();

        editTextRegistrationNumber = view.findViewById(R.id.editTextVehicleRegistrationNumber);
        editTextVehicleDescription = view.findViewById(R.id.editTextVehicleDescription);
        spinnerVehicleClass = view.findViewById(R.id.spinnerVehicleClass);
        editTextDrivingLicense = view.findViewById(R.id.editTextDrivingLicense);

        buttonVehicleRegistrationSave = view.findViewById(R.id.buttonVehicleRegistrationSave);
        buttonVehicleRegistrationUpdate = view.findViewById(R.id.buttonVehicleRegistrationUpdate);
        imageViewVehicleRegister = view.findViewById(R.id.imageViewVehicleRegister);
        checkboxHelmetPrompt = view.findViewById(R.id.checkboxHelmetPrompt);

        checkExistingVehicle();
        //spinner vehicle class
        final ArrayAdapter<String> spinnerClassArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item);
        spinnerClassArrayAdapter.addAll(getResources().getStringArray(R.array.vehicle_class));
        spinnerVehicleClass.setAdapter(spinnerClassArrayAdapter);

        spinnerVehicleClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i >= 0) {
                    vehicleClass = spinnerVehicleClass.getSelectedItem().toString();
                    switch (i) {
                        case 0:
                            imageViewVehicleRegister.setImageResource(R.drawable.ic_scooter_vehicle_register);
                            break;
                        case 1:
                            imageViewVehicleRegister.setImageResource(R.drawable.ic_regular_motorbike);
                            break;
                        case 2:
                            imageViewVehicleRegister.setImageResource(R.drawable.ic_sports_bike);
                            break;
                        case 3:
                            imageViewVehicleRegister.setImageResource(R.drawable.ic_electric_scooter);
                            break;
                        default:
                            imageViewVehicleRegister.setImageResource(R.drawable.ic_scooter_vehicle_register);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        buttonVehicleRegistrationSave.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                if (verifyFields()) {
                    drivingLicenseNumber = editTextDrivingLicense.getText().toString().trim();

                    boolean fieldCheck = vehicleVerification();
                    if (fieldCheck) {
                        try {
                            if (!drivingLicenseList.isDrivingLicenseValid(getActivity(), drivingLicenseNumber)) {
                                Toast.makeText(getActivity(), "Driving License is Invalid!", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if(drivingLicenseList.isDrivingLicenseValid(getActivity(), drivingLicenseNumber)){
                                setNewVehicle(editTextRegistrationNumber.getText().toString().trim(), editTextVehicleDescription.getText().toString().trim(),
                                        vehicleClass, checkboxHelmetPrompt.isChecked(), drivingLicenseNumber);
                                //showing success dialog
                                new LovelyStandardDialog(getActivity(), LovelyStandardDialog.ButtonLayout.VERTICAL)
                                        .setTopColor(R.color.colorGray)
                                        .setButtonsColorRes(R.color.text_color)
                                        .setIcon(R.drawable.ic_motorbike)
                                        .setTitle(R.string.view_vehicle_registration)
                                        .setMessage(R.string.vehicle_save_success)
                                        .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                Intent intent = new Intent(getActivity(), RiderMainActivity.class);
                                                startActivity(intent);
                                                getActivity().getFragmentManager().popBackStack();
                                            }
                                        })
                                        .show();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }


                }
            }
        });

        buttonVehicleRegistrationUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(verifyFields()){
                    updateVehicleDetails(editTextRegistrationNumber.getText().toString().trim(),
                                        editTextVehicleDescription.getText().toString().trim(),
                                        vehicleClass, checkboxHelmetPrompt.isChecked());
                }
            }
        });
        return view;
    }

    protected Boolean verifyFields() {
        if (TextUtils.isEmpty(editTextRegistrationNumber.getText().toString().trim())) {
            editTextRegistrationNumber.setError(getString(R.string.registration_error));
            return false;
        }
        if (TextUtils.isEmpty(editTextVehicleDescription.getText().toString().trim())) {
            editTextVehicleDescription.setError(getString(R.string.vehicle_desc_error));
            return false;
        }
        if (TextUtils.isEmpty(editTextDrivingLicense.getText().toString().trim())) {
            editTextDrivingLicense.setError("Enter Driving License Number");
            return false;
        }
        return true;
    }

    /* Check if vehicle already exists */
    protected void checkExistingVehicle() {
        try {
            databaseReference.child("users")
                    .child("riders")
                    .child(docID)
                    .child("vehicle_details")
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getChildrenCount() == 1) {
                                Log.d(TAG, "=====Fetching Vehicle Details=====");
                                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                    buttonVehicleRegistrationSave.setVisibility(View.GONE);
                                    buttonVehicleRegistrationUpdate.setVisibility(View.VISIBLE);
                                    String registrationNumber = Objects.requireNonNull(postSnapshot.child("vehicleRegistrationNumber").getValue()).toString();
                                    String DLNumber = Objects.requireNonNull(postSnapshot.child("vehicleDLNumber").getValue()).toString();
                                    String vehicleDesc = Objects.requireNonNull(postSnapshot.child("vehicleDesc").getValue()).toString();
                                    Boolean pillionHelmet = Objects.requireNonNull((Boolean) postSnapshot.child("pillionHelmet").getValue());
                                    String vehicleClassLocal = Objects.requireNonNull(postSnapshot.child("vehicleClass").getValue()).toString();

                                    editTextRegistrationNumber.setText(getString(R.string.blank_string, registrationNumber));
                                    editTextVehicleDescription.setText(getString(R.string.blank_string, vehicleDesc));
                                    if (pillionHelmet != null && pillionHelmet) {
                                        checkboxHelmetPrompt.setChecked(true);
                                    }
                                    spinnerVehicleClass.setSelection(getIndex(spinnerVehicleClass, vehicleClassLocal));
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Toast.makeText(getActivity(), "Firebase error occurred. Check internet connection and try again.", Toast.LENGTH_SHORT).show();
                        }
                    });
            }
            catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    //private method of your class
    private int getIndex(Spinner spinner, String myString){
        for(int i=0;i<spinner.getCount();i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }
        return -1;
    }

    protected void setNewVehicle(String registrationNumber, String vehicleDesc, String vehicleClass, Boolean helmetPrompt, String drivingLicenseNumber) {

        AddVehicleDetails addVehicleDetailsClass = new AddVehicleDetails(registrationNumber, vehicleDesc, vehicleClass, helmetPrompt, drivingLicenseNumber);
        DatabaseReference rootRef = databaseReference.child("users");
        DatabaseReference subRootRef = rootRef.child("riders").child(docID).child("vehicle_details");
        final DatabaseReference commuteRef = subRootRef.push();

        commuteRef.setValue(addVehicleDetailsClass)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "" + e.getMessage() + "");
                        Toast.makeText(getActivity(), "Could not add vehicle details. Please try again later", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    protected boolean vehicleVerification() {
        if (vehicleClass == null) {
            spinnerVehicleClass.setPrompt("Select Vehicle Class");
            return false;
        }
        return true;
    }
    protected void updateVehicleDetails(final String registrationNumber, final String vehicleDesc, final String vehicleClass, final Boolean helmetPrompt){
        AddVehicleDetails addVehicleDetailsClass = new AddVehicleDetails(registrationNumber, vehicleDesc, vehicleClass, helmetPrompt);
        DatabaseReference rootRef = databaseReference.child("users");
        final DatabaseReference subRootRef = rootRef.child("riders").child(docID).child("vehicle_details");
        subRootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    HashMap<String, Object> vehicleMap = new HashMap<>();
                        vehicleMap.put("vehicleRegistrationNumber",registrationNumber);
                        vehicleMap.put("vehicleDesc",vehicleDesc);
                        vehicleMap.put("vehicleClass",vehicleClass);
                        vehicleMap.put("pillionHelmet",helmetPrompt);
                        postSnapshot.getRef().updateChildren(vehicleMap);

                    new LovelyStandardDialog(getActivity(), LovelyStandardDialog.ButtonLayout.VERTICAL)
                            .setTopColor(R.color.colorGray)
                            .setButtonsColorRes(R.color.text_color)
                            .setIcon(R.drawable.ic_motorbike)
                            .setTitle(R.string.view_vehicle_registration)
                            .setMessage(R.string.vehicle_update_success)
                            .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(getActivity(), RiderMainActivity.class);
                                    startActivity(intent);
                                    getActivity().getFragmentManager().popBackStack();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
