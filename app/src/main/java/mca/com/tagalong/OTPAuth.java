package mca.com.tagalong;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import mca.com.tagalong.mca.com.classes.AddPassenger;
import mca.com.tagalong.mca.com.classes.AddRider;
import mca.com.tagalong.mca.com.classes.UserInformation;

public class OTPAuth extends AppCompatActivity {

    protected static final String TAG = "OTP";
    protected ActionBar actionBar;
    protected PinView pinView;
    protected TextView textViewMobile;
    protected TextView textViewError;
    protected TextView textViewSecondsRemaining;
    protected Button buttonSendOTP;
    protected Button buttonResendOTP;
    protected Button buttonVerifyOTP;
    protected FirebaseAuth firebaseAuth;
    protected DatabaseReference databaseReference;
    protected String SF_NAME = "docID";
    protected List<String> docIdList = new ArrayList<>();
    protected PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    protected String mVerificationId;
    protected PhoneAuthProvider.ForceResendingToken mResendToken;
    protected String mobileNumber;
    protected String otpNumber;
    protected ActionBar actionbar;
    protected TextView textTitle;
    protected android.app.ActionBar.LayoutParams layoutparams;
    UserInformation userInformationClass;

    private void ActionBarTitleGravity() {
        actionbar = getSupportActionBar();
        textTitle = new TextView(getApplicationContext());
        layoutparams = new android.app.ActionBar.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        textTitle.setLayoutParams(layoutparams);
        textTitle.setText(R.string.phone_verification_title);
        textTitle.setTextColor(Color.WHITE);
        textTitle.setTextSize(17);
        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionbar.setCustomView(textTitle);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpauth);
        ActionBarTitleGravity();
        actionbar.setDisplayHomeAsUpEnabled(true);
        userInformationClass = new UserInformation();

        textViewMobile = findViewById(R.id.textViewMobile);
        pinView = findViewById(R.id.firstPinView);
        textViewError = findViewById(R.id.textViewError);
        textViewSecondsRemaining = findViewById(R.id.textViewSecondsRemaining);
        buttonSendOTP = findViewById(R.id.buttonSendOTP);
        buttonResendOTP = findViewById(R.id.buttonResendOTP);
        buttonVerifyOTP = findViewById(R.id.buttonVerifyOTP);

        /* Firebase */
        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        Intent OTPIntent = this.getIntent();
        if (OTPIntent != null) {
            mobileNumber = OTPIntent.getExtras().getString("mobileNumber");
            mobileNumber = "+91" + mobileNumber;
            textViewMobile.setText(mobileNumber);
        }

        /* OTP Send Button */

        buttonSendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonSendOTP.setEnabled(false);
                buttonSendOTP.setVisibility(View.GONE);
                buttonVerifyOTP.setEnabled(true);
                buttonVerifyOTP.setVisibility(View.VISIBLE);

                //set countdown timer
                new CountDownTimer(30000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        textViewSecondsRemaining.setText(" " + millisUntilFinished / 1000);
                    }

                    public void onFinish() {
                        textViewSecondsRemaining.setText(" ");
                        buttonResendOTP.setEnabled(true);
                        buttonResendOTP.setVisibility(View.VISIBLE);
                    }
                }.start();

                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        textViewMobile.getText().toString().trim(),
                        30,
                        TimeUnit.SECONDS,
                        OTPAuth.this,
                        mCallbacks
                );
            }
        });

        buttonResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonResendOTP.setEnabled(false);
                textViewError.setVisibility(View.GONE);
                resendVerificationCode(textViewMobile.getText().toString().trim(), mResendToken);
            }
        });

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.d(TAG, e.getMessage());
                textViewError.setText(R.string.connection_error);
                textViewError.setVisibility(View.VISIBLE);
                textViewSecondsRemaining.setText(" ");
                textViewSecondsRemaining.setVisibility(View.GONE);
                buttonResendOTP.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                mVerificationId = verificationId;
                mResendToken = token;
                buttonVerifyOTP.setVisibility(View.VISIBLE);
                buttonSendOTP.setVisibility(View.GONE);
            }
        };

        buttonVerifyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpNumber = pinView.getText().toString().trim();
                if (otpNumber.length() < 6) {
                    textViewError.setError(getText(R.string.enter_otp_error));
                    return;
                }

                String verificationCode = otpNumber;
                if (verificationCode.equals("") || (verificationCode.isEmpty())) {
                    textViewError.setError(getText(R.string.enter_otp_error));
                } else {
                    PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.getCredential(mVerificationId, verificationCode);
                    signInWithPhoneAuthCredential(phoneAuthCredential);
                    buttonVerifyOTP.setEnabled(false);
                    buttonResendOTP.setEnabled(false);
                }
            }
        });
    }

    /* Phone Auth Credential Check */
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            boolean fromLoginActivity = getIntent().getExtras().getBoolean("fromLoginActivity");
                            if (!fromLoginActivity) {
                                boolean passengerRegister = getIntent().getExtras().getBoolean("passengerRegister");
                                if (passengerRegister) {
                                    writeNewPassenger(
                                            getIntent().getExtras().getString("passengerFirstName"),
                                            getIntent().getExtras().getString("passengerLastName"),
                                            getIntent().getExtras().getString("mobileNumber"),
                                            getIntent().getExtras().getString("passengerDOB"),
                                            getIntent().getExtras().getString("passengerGender")
                                    );
                                }
                                if (!passengerRegister) {
                                    writeNewRider(
                                            getIntent().getExtras().getString("firstName"),
                                            getIntent().getExtras().getString("lastName"),
                                            getIntent().getExtras().getString("mobileNumber"),
                                            getIntent().getExtras().getString("dob"),
                                            getIntent().getExtras().getString("gender"),
                                            getIntent().getExtras().getString("address1"),
                                            getIntent().getExtras().getString("address2"),
                                            getIntent().getExtras().getString("zipCode"),
                                            getIntent().getExtras().getString("state"),
                                            getIntent().getExtras().getString("city")
                                    );
                                }
                            } else {
                                /* From LoginActivity == true */
                                final String phone = getIntent().getExtras().getString("mobileNumber");
                                //check in rider node
                                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                                DatabaseReference userRef = databaseReference.child("users");
                                DatabaseReference riderRef = userRef.child("riders");
                                riderRef.orderByChild("contactNumber").equalTo(phone)
                                        .addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                if (dataSnapshot.getChildrenCount() > 0) {
                                                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                                        docIdList.add(postSnapshot.getKey());
                                                    }
                                                    Toast.makeText(getApplicationContext(), "Rider", Toast.LENGTH_SHORT).show();
                                                    final String docID = String.valueOf(docIdList.get(0));
                                                    SharedPreferences sharedPreferences = getSharedPreferences(SF_NAME, Context.MODE_PRIVATE);
                                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                                    editor.putString("documentID", docID);
                                                    editor.apply();
                                                    editor.commit();
                                                    Intent intent = new Intent(getApplicationContext(), RiderMainActivity.class);
                                                    intent.putExtra("contactNumber", phone);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                                Log.d("OTP_ERROR", databaseError.getMessage());
                                                Toast.makeText(getApplicationContext(), "Firebase error occurred. Check internet connection and try again.", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                //check in passenger node
                                DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference();
                                DatabaseReference userRef1 = databaseReference1.child("users");
                                DatabaseReference passengerRef = userRef1.child("passengers");
                                passengerRef.orderByChild("contactNumber").equalTo(phone)
                                        .addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                if (dataSnapshot.getChildrenCount() > 0) {
                                                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                                        docIdList.add(postSnapshot.getKey());
                                                    }
                                                    Toast.makeText(getApplicationContext(), "Passenger", Toast.LENGTH_SHORT).show();
                                                    final String docID = String.valueOf(docIdList.get(0));
                                                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SF_NAME, Context.MODE_PRIVATE);
                                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                                    editor.putString("documentID", docID);
                                                    editor.apply();
                                                    editor.commit();
                                                    Intent intent = new Intent(getApplicationContext(), PassengerMainActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                } else {
                                                    Toast.makeText(getApplicationContext(), "User not found", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                Log.d("OTP_ERROR", databaseError.getMessage());
                                                Toast.makeText(getApplicationContext(), "Firebase error occurred. Check internet connection and try again.", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                            }

                        } else {
                            // Sign in failed, display a message and update the UI
                            textViewError.setText(R.string.invalid_otp_enter);
                            textViewError.setVisibility(View.VISIBLE);
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(getApplicationContext(), "Firebase Error. Check Again After Sometime", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }

    /* Resend OTP */
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                textViewMobile.getText().toString().trim(),        // Phone number to verify
                30,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    private void writeNewRider(String firstName, String lastName, String contactNumber, String dob, String gender, String address1, String address2, String zipCode, String state, String city) {
        AddRider rider = new AddRider(firstName, lastName, contactNumber, dob, gender, address1, address2, zipCode, state, city);
        DatabaseReference rootRef = databaseReference.child("users");
        DatabaseReference subRootRef = rootRef.child("riders");
        final DatabaseReference riderRef = subRootRef.push();

        riderRef.setValue(rider)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        SharedPreferences pref = getSharedPreferences(SF_NAME, MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("documentID", riderRef.getKey());
                        editor.apply();
                        Toast.makeText(getApplicationContext(), "Registration Success!", Toast.LENGTH_SHORT).show();
                        Intent riderIntent = new Intent(OTPAuth.this, RiderMainActivity.class);
                        startActivity(riderIntent);
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("ERR: ", "" + e.getMessage() + "");
                        Toast.makeText(getApplicationContext(), "Could not complete registration. Please try again later", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void writeNewPassenger(String firstName, String lastName, String mobileNumber, String dob, String gender) {

        AddPassenger addPassengerClass = new AddPassenger(firstName, lastName, mobileNumber, dob, gender);

        DatabaseReference rootRef = databaseReference.child("users");
        DatabaseReference subRootRef = rootRef.child("passengers");

        final DatabaseReference passengerRef = subRootRef.push();

        passengerRef.setValue(addPassengerClass)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        SharedPreferences pref = getSharedPreferences(SF_NAME, MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("documentID", passengerRef.getKey());
                        editor.apply();

                        Toast.makeText(getApplicationContext(), "Registration Success!", Toast.LENGTH_SHORT).show();
                        Intent passengerIntent = new Intent(OTPAuth.this, PassengerMainActivity.class);
                        startActivity(passengerIntent);
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("ERR: ", "" + e.getMessage() + "");
                        Toast.makeText(getApplicationContext(), "Could not complete registration. Please try again later", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent mainIntent = new Intent();
        mainIntent.setClass(OTPAuth.this, LoginActivity.class);
        startActivity(mainIntent);
        finish();
        return true;
    }
}