package mca.com.tagalong;


import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import mca.com.tagalong.mca.com.classes.DirectionsJSONParser;
import mca.com.tagalong.mca.com.classes.GMapV2Direction;

public class ViewCommuteRouteFragment extends Fragment implements OnMapReadyCallback {

    protected final String TAG = "VIEW_COMMUTE";
    protected String SF_NAME = "docID";
    protected SharedPreferences sharedpreferences;

    protected String docID;
    protected TextView textViewSource;
    protected TextView textViewDestination;
    protected TextView textStartTrip;
    protected Button buttonEditCommute;
    protected TextView textViewRiderName;
    protected Button buttonInviteTrip;
    protected Button buttonStartTrip;
    protected Button buttonCancelTrip;

    protected ArrayList<LatLng> markerPoints = new ArrayList<>();
    protected LatLng sourceLatLng, destinationLatLng;
    protected String sourceLat, sourceLang;
    protected String destinationLat, destinationLang;
    protected String tripInfo;
    private GoogleMap mMap;

    public ViewCommuteRouteFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_commute_route, container, false);
        textViewSource = view.findViewById(R.id.textSourcePoint);
        textViewDestination = view.findViewById(R.id.textDestinationPoint);
        textStartTrip = view.findViewById(R.id.textStartTrip);
        buttonEditCommute = view.findViewById(R.id.buttonEditCommute);
        textViewRiderName = view.findViewById(R.id.textViewRiderName);
        Button buttonInviteTrip = view.findViewById(R.id.buttonInviteTrip);
        Button buttonStartTrip = view.findViewById(R.id.buttonStartTrip);
        Button buttonCancelTrip = view.findViewById(R.id.buttonCancelTrip);

        sharedpreferences = getActivity().getSharedPreferences(SF_NAME,
                Context.MODE_PRIVATE);
        docID = sharedpreferences.getString("documentID", null);
        Log.d(TAG, "" + docID + "");

        loadRiderName();
        MapFragment mapFragment = (MapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        buttonEditCommute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("editSchedule", true);
                RiderSetCommuteFragment fragment = new RiderSetCommuteFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(((ViewGroup) Objects.requireNonNull(getView()).getParent()).getId(), fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        buttonStartTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        buttonInviteTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        buttonCancelTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Do you wish to delete existing schedule?")
                        .setTitle("Delete Schedule");
                builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                        DatabaseReference riderRef = databaseReference.child("users").child("riders");
                        final DatabaseReference commuteRef = riderRef.child(docID).child("commute_details");
                        commuteRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.getChildrenCount() > 0) {
                                    dataSnapshot.getRef().setValue(null);
                                    String contactNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                    Log.d(TAG, ""+contactNumber+"");
                                    Intent intent = new Intent(getActivity(), RiderMainActivity.class);
                                    intent.putExtra("contactNumber", contactNumber);
                                    startActivity(new Intent(getActivity(), RiderMainActivity.class));
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getActivity(), "Firebase Error. Please try again later.", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        return view;
    }

    protected void loadRiderName() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref = databaseReference.child("users");
        DatabaseReference riderRef = ref.child("riders");
        DatabaseReference snapRef = riderRef.child(docID);
        snapRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String firstName = dataSnapshot.child("firstName").getValue(String.class);
                String lastName = dataSnapshot.child("lastName").getValue(String.class);
                String fullName = firstName + " " + lastName;
                textViewRiderName.setText(getString(R.string.blank_string, fullName));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("EXCEPTION", "onCancelled", databaseError.toException());
                Toast.makeText(getActivity(), "Could not fetch details. Try again later", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //final LatLng source = new LatLng(13.048891300000001, 77.6142543);
        final LatLng source = new LatLng(12.9716, 77.5946); //Bangalore co-ord
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(source, 12));
        //Loading commute route
        final ProgressDialog progressDialog;
        progressDialog = ProgressDialog.show(getActivity(), "Loading Route", "Drawing Map..", true);
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref = databaseReference.child("users");
        final DatabaseReference commuteRef = ref.child("riders").child(docID).child("commute_details");
        commuteRef.addValueEventListener(new ValueEventListener() {
            @TargetApi(Build.VERSION_CODES.O)
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot commuteSnapshot : dataSnapshot.getChildren()) {
                    Log.d("childCount", String.valueOf(commuteSnapshot.getChildrenCount()));
                    String sourcePoint = commuteSnapshot.child("source").getValue(String.class);
                    String scheduleTime = commuteSnapshot.child("startTime").getValue(String.class);
                    String startDate = commuteSnapshot.child("startDate").getValue(String.class);
                    String endDate = commuteSnapshot.child("endDate").getValue(String.class);

                    List<String> sourceLangList = Arrays.asList(Objects.requireNonNull(sourcePoint).split(","));
                    sourceLat = sourcePoint.substring(10, 19);
                    sourceLang = sourceLangList.get(1).substring(0, 8);
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(getContext(), Locale.getDefault());
                    try {
                        String sPlace;
                        addresses = geocoder.getFromLocation(Double.parseDouble(sourceLat), Double.parseDouble(sourceLang), 1);
                        String address = addresses.get(0).getAddressLine(0);
                        String city = addresses.get(0).getAddressLine(1);
                        String country = addresses.get(0).getAddressLine(2);

                        textViewSource.setText(getString(R.string.blank_string, address));

                        String[] splitAddress = address.split(",");
                        sPlace = splitAddress[0] + "\n";
                        if (city != null && !city.isEmpty()) {
                            String[] splitCity = city.split(",");
                            sPlace += splitCity[0];
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String destinationPoint = commuteSnapshot.child("destination").getValue(String.class);
                    List<String> destinationLangList = Arrays.asList(destinationPoint.split(","));
                    destinationLat = destinationPoint.substring(10, 19);
                    destinationLang = destinationLangList.get(1).substring(0, 8);

                    Geocoder geocoder1;
                    List<Address> addresses1;
                    geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        String sPlace1;
                        addresses1 = geocoder.getFromLocation(Double.parseDouble(destinationLat), Double.parseDouble(destinationLang), 1);
                        String address1 = addresses1.get(0).getAddressLine(0);
                        String city1 = addresses1.get(0).getAddressLine(1);
                        String country1 = addresses1.get(0).getAddressLine(2);

                        textViewDestination.setText(getString(R.string.blank_string, address1));

                        String[] splitAddress1 = address1.split(",");
                        sPlace1 = splitAddress1[0] + "\n";
                        if (city1 != null && !city1.isEmpty()) {
                            String[] splitCity = city1.split(",");
                            sPlace1 += splitCity[0];
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // Adding new item to the ArrayList
                    sourceLatLng = new LatLng(Double.parseDouble(sourceLat), Double.parseDouble(sourceLang));
                    destinationLatLng = new LatLng(Double.parseDouble(destinationLat), Double.parseDouble(destinationLang));

                    //Calculating Distance between 2 points in KMS
                    Log.d(TAG, "" + destinationLat + "");
                    Log.d(TAG, "" + destinationLang + "");
                    Log.d(TAG, "" + sourceLat + "");
                    Log.d(TAG, "" + sourceLang + "");

                    Location sourceLocation = new Location("");
                    Location destinationLocation = new Location("");
                    sourceLocation.setLatitude(Double.parseDouble(sourceLat));
                    sourceLocation.setLongitude(Double.parseDouble(sourceLang));
                    destinationLocation.setLatitude(Double.parseDouble(destinationLat));
                    destinationLocation.setLongitude(Double.parseDouble(destinationLang));
                    float distanceInMeters = destinationLocation.distanceTo(sourceLocation);
                    float distanceInKms = distanceInMeters / 1000;

                    tripInfo = startDate + " => " + endDate + " : " + scheduleTime;

                    markerPoints.add(sourceLatLng);
                    markerPoints.add(destinationLatLng);

                    // Creating MarkerOptions
                    MarkerOptions options = new MarkerOptions();
                    MarkerOptions options1 = new MarkerOptions();

                    // Setting the position of the marker
                    options.position(sourceLatLng);
                    options1.position(destinationLatLng);

                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    options1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

                    // Add new marker to the Google Map Android API V2
                    mMap.addMarker(options);
                    mMap.addMarker(options1);

                    LatLng origin = sourceLatLng;
                    LatLng dest = destinationLatLng;

                    // Getting URL to the Google Directions API
                    String url = getDirectionsUrl(origin, dest);
                    DownloadTask downloadTask = new DownloadTask();
                    // Start downloading json data from Google Directions API
                    downloadTask.execute(url);

                    getDuration getDuration = new getDuration();
                    String[] distanceAddress = new String[5];
                    distanceAddress[0] = sourceLat;
                    distanceAddress[1] = sourceLang;
                    distanceAddress[2] = destinationLat;
                    distanceAddress[3] = destinationLang;
                    distanceAddress[4] = "driving";
                    getDuration.execute(distanceAddress);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("EXCEPTION", "onCancelled", databaseError.toException());
                Toast.makeText(getActivity(), "Could not fetch details. Try again later", Toast.LENGTH_SHORT).show();
            }
        });
        progressDialog.dismiss();

    }

    // Checks, whether start and end locations are captured
    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class getDuration extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            GMapV2Direction gMapV2Direction = new GMapV2Direction();
            Location sourceLocation = new Location("");
            Location destinationLocation = new Location("");
            sourceLocation.setLatitude(Double.parseDouble(strings[0]));
            sourceLocation.setLongitude(Double.parseDouble(strings[1]));
            destinationLocation.setLatitude(Double.parseDouble(strings[2]));
            destinationLocation.setLongitude(Double.parseDouble(strings[3]));

            Document document = gMapV2Direction.getDocument(sourceLocation, destinationLocation, "driving");
            Log.d(TAG, "" + document + "");
            Integer distance = gMapV2Direction.getDistanceValue(document);
            return String.valueOf(distance / 1000);
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(TAG, "" + result + " Kms.");
            tripInfo += " = " + result + " KMS.";
            textStartTrip.setText(getString(R.string.blank_string, tripInfo));
        }

    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);

            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.RED);
                lineOptions.geodesic(true);

            }

// Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
    }


}
