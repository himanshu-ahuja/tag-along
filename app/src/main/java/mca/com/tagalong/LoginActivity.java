package mca.com.tagalong;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import mca.com.tagalong.mca.com.classes.AddPassenger;
import mca.com.tagalong.mca.com.classes.AddRider;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LOGIN";
    private EditText phone;

    private DatabaseReference mDatabaseReference;

    private void ActionBarTitleGravity() {
        /* Actionbar */
        ActionBar actionbar = getSupportActionBar();
        TextView textTitle = new TextView(getApplicationContext());
        android.app.ActionBar.LayoutParams layoutparams = new android.app.ActionBar.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        textTitle.setLayoutParams(layoutparams);
        textTitle.setText(R.string.login);
        textTitle.setGravity(Gravity.CENTER);
        textTitle.setTextColor(Color.WHITE);
        textTitle.setTextSize(18);
        textTitle.setAllCaps(true);
        assert actionbar != null;
        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionbar.setCustomView(textTitle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ActionBarTitleGravity();
        Button signIn = findViewById(R.id.buttonLogin);
        Button signUp = findViewById(R.id.buttonRegister);
        phone = findViewById(R.id.editTextPhoneNumber);
        String mobileNumber = phone.getText().toString().trim();

        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage("Are you a Rider or Passenger?")
                        .setTitle("Registration");
                builder.setPositiveButton(R.string.passenger, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(LoginActivity.this, PassengerRegister.class));
                        finish();
                    }
                });

                builder.setNegativeButton(R.string.rider, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(LoginActivity.this, RiderRegister.class));
                        finish();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(phone.getText())) {
                    phone.setError(getText(R.string.error_phone_number_error));
                    return;
                }

                DatabaseReference userRef = mDatabaseReference.child("users");
                //check in passenger node
                DatabaseReference passengersRef = userRef.child("passengers");
                Query query =passengersRef.orderByChild("contactNumber").equalTo(phone.getText().toString().trim());
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("P_COUNT",String.valueOf(dataSnapshot.getChildrenCount()));
                        if(dataSnapshot.getChildrenCount() > 0){
                            Intent intent= new Intent(getApplicationContext(),OTPAuth.class);
                            intent.putExtra("fromLoginActivity",true);
                            intent.putExtra("mobileNumber",phone.getText().toString().trim());
                            startActivity(intent);
                            finish();
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d("CLASS", "Firebase Error");
                    }
                });
                //check in rider node
                DatabaseReference riderRef = userRef.child("riders");
                Query query1 =riderRef.orderByChild("contactNumber").equalTo(phone.getText().toString().trim());
                query1.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("R_COUNT",String.valueOf(dataSnapshot.getChildrenCount()));
                        if(dataSnapshot.getChildrenCount() > 0){
                            Intent intent= new Intent(getApplicationContext(),OTPAuth.class);
                            intent.putExtra("fromLoginActivity",true);
                            intent.putExtra("mobileNumber",phone.getText().toString().trim());
                            startActivity(intent);
                            finish();
                        }
                        if(dataSnapshot.getChildrenCount() < 0){
                            phone.setError("Account does not exist!");
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d("CLASS", "Firebase Error");
                    }
                });
            }
        });
    }
}
