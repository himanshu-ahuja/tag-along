package mca.com.tagalong;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.Calendar;

import mca.com.tagalong.mca.com.verification.AadharList;

public class RiderRegister extends AppCompatActivity {

    /* Log */
    private static final String TAG_AADHAR = "AADHAR";
    /* Actionbar */
    protected ActionBar actionbar;
    protected TextView textTitle;

    /* Calendar */
    protected android.app.ActionBar.LayoutParams layoutparams;
    protected Calendar dateSelected = Calendar.getInstance();
    protected long maxDateLong;
    protected long minDateLong;
    protected Button buttonNext;
    /* Views */
    protected EditText editTextAadharNumber;
    protected EditText editTextFirstName;
    protected EditText editTextLastName;
    protected EditText editTextDOB;
    protected EditText editTextAddress_1;
    protected EditText editTextAddress_2;
    protected EditText editTextPinCode;
    protected MaterialSpinner spinnerState;
    protected MaterialSpinner spinnerCity;
    protected RadioButton radioButtonMale;
    protected RadioButton radioButtonFemale;
    protected DatePickerDialog datePickerDialog;
    protected String aadharNumber;
    protected String phoneNumber;
    protected String gender;
    protected String state;
    protected String city;

    /* Firebase */
    protected FirebaseAuth firebaseAuth;
    protected DatabaseReference mDatabaseReference;

    public Boolean getResultAadhar() {
        return resultAadhar;
    }

    public void setResultAadhar(Boolean resultAadhar) {
        this.resultAadhar = resultAadhar;
    }

    protected Boolean resultAadhar;

    private void ActionBarTitleGravity() {
        actionbar = getSupportActionBar();
        textTitle = new TextView(getApplicationContext());
        layoutparams = new android.app.ActionBar.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        textTitle.setLayoutParams(layoutparams);
        textTitle.setText(R.string.rider_registration);
        textTitle.setTextColor(Color.WHITE);
        textTitle.setTextSize(17);
        textTitle.setAllCaps(true);
        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionbar.setCustomView(textTitle);
        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Window window = RiderRegister.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(RiderRegister.this, R.color.colorPrimary));
        setContentView(R.layout.activity_rider_register);
        ActionBarTitleGravity();

        firebaseAuth = FirebaseAuth.getInstance();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        editTextAadharNumber = findViewById(R.id.editTextAadharNumber);
        editTextFirstName = findViewById(R.id.riderFirstName);
        editTextLastName = findViewById(R.id.riderLastName);
        editTextDOB = findViewById(R.id.textRiderDOB);
        editTextAddress_1 = findViewById(R.id.textRiderAddressLocal);
        editTextAddress_2 = findViewById(R.id.textRiderAddressLine2);
        editTextPinCode = findViewById(R.id.textRiderPinCode);
        spinnerState = findViewById(R.id.spinnerRiderState);
        spinnerCity = findViewById(R.id.spinnerRiderCity);
        radioButtonMale = findViewById(R.id.radioButtonMale);
        radioButtonFemale = findViewById(R.id.radioButtonFemale);
        buttonNext = findViewById(R.id.buttonRiderRegisterNext);

        //Spinner State
        ArrayAdapter<String> spinnerStateArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item);
        spinnerStateArrayAdapter.add("Select State");
        spinnerStateArrayAdapter.addAll(getResources().getStringArray(R.array.states));
        spinnerState.setAdapter(spinnerStateArrayAdapter);

        spinnerState.setAdapter(spinnerStateArrayAdapter);
        spinnerState.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                if (position > 0) {
                    state = item.toString();
                }
            }
        });

        //Spinner City
        ArrayAdapter<String> spinnerCityArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item);
        spinnerCityArrayAdapter.add("Select City");
        spinnerCityArrayAdapter.addAll(getResources().getStringArray(R.array.cities));
        spinnerCity.setAdapter(spinnerCityArrayAdapter);

        spinnerCity.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                if (position > 0) {
                    city = item.toString();
                }
            }
        });

        /* Date */
        final Calendar cal = Calendar.getInstance();
        Calendar min = Calendar.getInstance();
        Calendar threemonths = Calendar.getInstance();
        threemonths.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) - 2, cal.get(Calendar.DAY_OF_MONTH));
        min.set(1970, 0, 01);
        minDateLong = min.getTimeInMillis();
        maxDateLong = threemonths.getTimeInMillis();
        final DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int yearSelected = year;
                int monthSelected = month + 1;
                int daySelected = dayOfMonth;
                editTextDOB.setText("" + dayOfMonth + "/" + monthSelected + "/" + yearSelected + "");
            }

        };

        final DatePickerDialog datePickerDialog = new DatePickerDialog(RiderRegister.this,
                dateListener,   //set the listener
                cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(minDateLong);
        datePickerDialog.getDatePicker().setMaxDate(maxDateLong);

        editTextDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aadharVerify();
                if(getResultAadhar()){
                    proceed();
                }
            }
        });
    }

    public void aadharVerify(){

        aadharNumber = editTextAadharNumber.getText().toString().trim();
        final AadharList aadharListClass = new AadharList();
        setResultAadhar(true);
        try {
            boolean valid = aadharListClass.isAadharValid(getApplicationContext(), aadharNumber);
            if (valid) {
                phoneNumber = aadharListClass.getAadharValue(aadharNumber);
                //check if aadhar is already used
                DatabaseReference userRef = mDatabaseReference.child("users");
                //check in passenger node
                DatabaseReference passengersRef = userRef.child("passengers");
                Query query = passengersRef.orderByChild("contactNumber").equalTo(phoneNumber);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("P_COUNT", String.valueOf(dataSnapshot.getChildrenCount()));
                        if (dataSnapshot.getChildrenCount() > 0) {
                            editTextAadharNumber.setError("Account already exist!");
                            setResultAadhar(false);
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d("CLASS", "Firebase Error");
                    }
                });

                //check in rider node
                DatabaseReference riderRef = userRef.child("riders");
                Query query1 = riderRef.orderByChild("contactNumber").equalTo(phoneNumber);
                query1.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("R_COUNT", String.valueOf(dataSnapshot.getChildrenCount()));
                        if (dataSnapshot.getChildrenCount() > 0) {
                            editTextAadharNumber.setError("Account already exist!");
                            setResultAadhar(false);
                        }
                        else{
                            setResultAadhar(true);
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d("CLASS", "Firebase Error");
                    }
                });

            } else {
                editTextAadharNumber.setError("Invalid Aadhar Number");
            }

        } catch (Exception e) {
            Log.d(TAG_AADHAR, "Error in processing Aadhar Info.");
            e.printStackTrace();
        }
    }

    public void proceed() {
        //create user
        boolean validation = validInput();

        if (validation) {
            /* Phone Number Confirmation */
            AlertDialog.Builder builder = new AlertDialog.Builder(RiderRegister.this);
            builder.setMessage("+91 " + phoneNumber)
                    .setTitle(R.string.phone_verification_title);
            builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Clicked Confirm
                    final ProgressDialog progressDialog;
                    progressDialog = ProgressDialog.show(RiderRegister.this, "Registration", "Creating Rider..", true);

                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    Intent intent = new Intent();
                    intent.setClass(RiderRegister.this, OTPAuth.class);
                    intent.putExtra("firstName", editTextFirstName.getText().toString());
                    intent.putExtra("lastName", editTextLastName.getText().toString());
                    intent.putExtra("mobileNumber", phoneNumber);
                    intent.putExtra("dob", editTextDOB.getText().toString());
                    intent.putExtra("gender", gender);
                    intent.putExtra("address1", editTextAddress_1.getText().toString());
                    intent.putExtra("address2", editTextAddress_2.getText().toString());
                    intent.putExtra("zipCode", editTextPinCode.getText().toString());
                    intent.putExtra("state", state);
                    intent.putExtra("city", city);
                    intent.putExtra("passengerRegister", false);
                    intent.putExtra("fromLoginActivity", false);
                    startActivity(intent);
                    finish();

                }
            });

            builder.setNegativeButton(R.string.edit, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Clicked Edit
                    editTextAadharNumber.setFocusable(true);
                    editTextAadharNumber.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editTextAadharNumber, InputMethodManager.SHOW_IMPLICIT);
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

        }
    }

    public boolean validInput() {
        if (TextUtils.isEmpty(editTextFirstName.getText())) {
            editTextFirstName.setError("First Name is required");
            return false;
        }
        if (TextUtils.isEmpty(editTextAadharNumber.getText())) {
            editTextAadharNumber.setError("Aadhar Number is required");
            return false;
        }
        if (TextUtils.isEmpty(editTextDOB.getText())) {
            editTextDOB.setError("Date of Birth is required");
            return false;
        }
        if (TextUtils.isEmpty(editTextAddress_1.getText())) {
            editTextAddress_1.setError("Address is required");
            return false;
        }
        if (TextUtils.isEmpty(editTextPinCode.getText())) {
            editTextPinCode.setError("Zip code is required");
            return false;
        }

        if (state == null) {
            Toast.makeText(getApplicationContext(), "Select State", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (city == null) {
            Toast.makeText(getApplicationContext(), "Select City", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!(radioButtonMale.isChecked() || radioButtonFemale.isChecked())) {
            Toast.makeText(getApplicationContext(), "Select Gender", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (radioButtonMale.isChecked()) {
            gender = "Male";
        }
        if (radioButtonFemale.isChecked()) {
            gender = "Female";
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(RiderRegister.this, LoginActivity.class));
        finish();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
