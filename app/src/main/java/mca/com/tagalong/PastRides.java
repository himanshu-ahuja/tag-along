package mca.com.tagalong;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class PastRides extends Fragment {

    public PastRides() {
    }

    CardView cardView1;
    CardView cardView2;
    TextView textViewName1;
    TextView textViewContact1;
    TextView textViewEarnings1;

    TextView textViewName2;
    TextView textViewContact2;
    TextView textViewEarnings2;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_past_rides, container, false);

        cardView1 = view.findViewById(R.id.cardView1);
        cardView2 = view.findViewById(R.id.cardView2);

        textViewName1 = view.findViewById(R.id.textViewRiderName1);
        textViewName2 = view.findViewById(R.id.textViewRiderName2);

        textViewContact1 = view.findViewById(R.id.textViewRiderContactNumber1);
        textViewContact2 = view.findViewById(R.id.textViewRiderContactNumber2);

        textViewEarnings1 = view.findViewById(R.id.textViewRiderGender1);
        textViewEarnings2 = view.findViewById(R.id.textViewRiderGender2);


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference historyRef = databaseReference.child("history");
        historyRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount() > 0) {
                    cardView1.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), "Unable to fetch details {Firebase Error}. Try again after some time", Toast.LENGTH_SHORT).show();
                }
                if(!dataSnapshot.exists()){
                    cardView1.setVisibility(View.INVISIBLE);
                    cardView2.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Unable to fetch details {Firebase Error}. Try again after some time", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

}
