package mca.com.tagalong;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

public class PassengerRegister extends AppCompatActivity {

    private static final String TAG = "REGISTER_P";
    /* Actionbar */
    protected ActionBar actionbar;
    protected TextView textTitle;

    /* Calendar */
    protected android.app.ActionBar.LayoutParams layoutparams;
    protected Calendar dateSelected = Calendar.getInstance();
    protected long maxDateLong;
    protected long minDateLong;
    protected Button buttonNext;

    /* Views */
    protected EditText editTextFirstName;
    protected EditText editTextLastName;
    protected EditText editTextDOB;
    protected EditText editTextContactNumber;

    protected RadioButton radioButtonMale;
    protected RadioButton radioButtonFemale;
    protected DatePickerDialog datePickerDialog;
    protected String phoneNumber;
    protected String gender;

    /* Firebase */
    protected FirebaseAuth mFirebaseAuth;
    protected DatabaseReference mDatabaseReference;

    private void ActionBarTitleGravity() {
        actionbar = getSupportActionBar();
        textTitle = new TextView(getApplicationContext());
        layoutparams = new android.app.ActionBar.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        textTitle.setLayoutParams(layoutparams);
        textTitle.setText(R.string.passenger_registration);
        textTitle.setTextColor(Color.WHITE);
        textTitle.setTextSize(17);
        textTitle.setAllCaps(true);
        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionbar.setCustomView(textTitle);
        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Window window = PassengerRegister.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(PassengerRegister.this, R.color.colorPrimary));
        setContentView(R.layout.activity_passenger_register);
        ActionBarTitleGravity();

        mFirebaseAuth= FirebaseAuth.getInstance();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        editTextFirstName = findViewById(R.id.PassengerFirstName);
        editTextLastName = findViewById(R.id.PassengerLastName);
        editTextDOB = findViewById(R.id.textPassengerDOB);
        editTextContactNumber = findViewById(R.id.editTextPassengerContactNumber);
        radioButtonMale = findViewById(R.id.radioButtonMale);
        radioButtonFemale = findViewById(R.id.radioButtonFemale);
        buttonNext = findViewById(R.id.buttonPassengerRegisterNext);

        //init calendar to get the current year, month and day
        final Calendar cal = Calendar.getInstance();
        Calendar min = Calendar.getInstance();
        Calendar threemonths = Calendar.getInstance();
        threemonths.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) - 2, cal.get(Calendar.DAY_OF_MONTH));
        min.set(1970, 0, 01);
        minDateLong = min.getTimeInMillis();
        maxDateLong = threemonths.getTimeInMillis();
        final DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int yearSelected = year;
                int monthSelected = month + 1;
                int daySelected = dayOfMonth;
                editTextDOB.setText("" + dayOfMonth + "/" + monthSelected + "/" + yearSelected + "");
            }

        };

        final DatePickerDialog datePickerDialog = new DatePickerDialog(PassengerRegister.this,
                dateListener,   //set the listener
                cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(minDateLong);
        datePickerDialog.getDatePicker().setMaxDate(maxDateLong);
        editTextDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //check if existing
                DatabaseReference userRef = mDatabaseReference.child("users");
                //check in rider node
                DatabaseReference riderRef = userRef.child("riders");
                Query query1 = riderRef.orderByChild("contactNumber").equalTo(editTextContactNumber.getText().toString().trim());
                query1.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("R_COUNT", String.valueOf(dataSnapshot.getChildrenCount()));
                        if (dataSnapshot.getChildrenCount() > 0) {
                            editTextContactNumber.setError("Account already exist!");
                            return;
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d("CLASS", "Firebase Error");
                    }
                });

                //check in passenger node
                DatabaseReference passengersRef = userRef.child("passengers");
                Query query = passengersRef.orderByChild("contactNumber").equalTo(editTextContactNumber.getText().toString().trim());
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("P_COUNT", String.valueOf(dataSnapshot.getChildrenCount()));
                        if (dataSnapshot.getChildrenCount() > 0) {
                            editTextContactNumber.setError("Account already exist!");
                        }
                        if(dataSnapshot.getChildrenCount() <= 0){
                            /* Validating Fields */
                            Boolean validation = validInput();
                            if (validation) {
                                phoneNumber = editTextContactNumber.getText().toString().trim();
                                /* Phone Number Confirmation */
                                AlertDialog.Builder builder = new AlertDialog.Builder(PassengerRegister.this);
                                builder.setMessage("+91 " + phoneNumber)
                                        .setTitle(R.string.phone_verification_title);
                                builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Clicked Confirm
                                        final ProgressDialog progressDialog;
                                        progressDialog = ProgressDialog.show(PassengerRegister.this, "Registration", "Creating Account..", true);
                                            if (progressDialog != null && progressDialog.isShowing())
                                                progressDialog.dismiss();
                                            Intent intent = new Intent();
                                            intent.setClass(PassengerRegister.this, OTPAuth.class);
                                            intent.putExtra("firstName", editTextFirstName.getText().toString());
                                            intent.putExtra("lastName", editTextLastName.getText().toString());
                                            intent.putExtra("mobileNumber", phoneNumber);
                                            intent.putExtra("dob", editTextDOB.getText().toString());
                                            intent.putExtra("gender", gender);
                                            intent.putExtra("passengerRegister", true);
                                            intent.putExtra("fromLoginActivity", false);
                                            startActivity(intent);
                                            finish();
                                    }
                                });

                                builder.setNegativeButton(R.string.edit, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Clicked Edit
                                        editTextContactNumber.setFocusable(true);
                                        editTextContactNumber.requestFocus();
                                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.showSoftInput(editTextContactNumber, InputMethodManager.SHOW_IMPLICIT);
                                    }
                                });

                                AlertDialog dialog = builder.create();
                                dialog.show();

                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d("CLASS", "Firebase Error");
                    }
                });



            }
        });
    }

    public Boolean validInput() {
        if (TextUtils.isEmpty(editTextFirstName.getText())) {
            editTextFirstName.setError("First Name is required");
            return false;
        }
        if (TextUtils.isEmpty(editTextContactNumber.getText())) {
            editTextContactNumber.setError("Contact Number is required");
            return false;
        }
        if (TextUtils.isEmpty(editTextDOB.getText())) {
            editTextDOB.setError("Date of Birth is required");
            return false;
        }

        if (!(radioButtonMale.isChecked() || radioButtonFemale.isChecked())) {
            Toast.makeText(getApplicationContext(), "Select Gender", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (radioButtonMale.isChecked()) {
            gender = "Male";
        }
        if (radioButtonFemale.isChecked()) {
            gender = "Female";
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(PassengerRegister.this, LoginActivity.class));
        finish();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}

