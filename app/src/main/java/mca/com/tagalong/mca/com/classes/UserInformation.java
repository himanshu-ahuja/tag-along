/*
 * This class will fetch the document ID of the user if account exists in Firebase
 * */

package mca.com.tagalong.mca.com.classes;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class UserInformation {

    private List<String> docIdList = new ArrayList<>();
    private String SF_NAME = "docID";
    private Boolean passenger = false;
    private Boolean rider = false;

    public boolean isRider(final Context context, final String phoneNumber) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference userRef = databaseReference.child("users");
        DatabaseReference riderRef = userRef.child("riders");
        riderRef.orderByChild("contactNumber").equalTo(phoneNumber)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getChildrenCount() > 0) {
                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                docIdList.add(postSnapshot.getKey());
                            }
                            Toast.makeText(context, "Rider", Toast.LENGTH_SHORT).show();
                            final String docID = String.valueOf(docIdList.get(0));
                            SharedPreferences sharedPreferences = context.getSharedPreferences(SF_NAME, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("documentID", docID);
                            editor.apply();
                            editor.commit();
                            rider = true;
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d("OTP_ERROR", databaseError.getMessage());
                        Toast.makeText(context, "Firebase error occurred. Check internet connection and try again.", Toast.LENGTH_SHORT).show();
                    }
                });
        return rider;
    }

    public boolean isPassenger(final Context context, String phoneNumber) {
        /* Check in other node */
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference userRef = databaseReference.child("users");
        DatabaseReference passengerRef = userRef.child("passengers");
        passengerRef.orderByChild("contactNumber").equalTo(phoneNumber)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getChildrenCount() > 0) {
                                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                    docIdList.add(postSnapshot.getKey());
                                }
                                Toast.makeText(context, "Passenger", Toast.LENGTH_SHORT).show();
                                final String docID = String.valueOf(docIdList.get(0));
                                SharedPreferences sharedPreferences = context.getSharedPreferences(SF_NAME, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("documentID", docID);
                                editor.apply();
                                editor.commit();
                                passenger = true;
                            } else {
                               Toast.makeText(context, "User not found.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d("OTP_ERROR",databaseError.getMessage());
                        Toast.makeText(context, "Firebase error occurred. Check internet connection and try again.", Toast.LENGTH_SHORT).show();
                    }
                });
        return passenger;
    }

}
