package mca.com.tagalong.mca.com.classes;

public class SetCommuteRoute {

    public SetCommuteRoute(){}

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getSelectedVehicle() {
        return selectedVehicle;
    }

    public void setSelectedVehicle(String selectedVehicle) {
        this.selectedVehicle = selectedVehicle;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String startDate;
    private String endDate;
    private String source;
    private String destination;
    private String startTime;
    private String selectedVehicle;
    private String status;

    public SetCommuteRoute(String startDate, String endDate, String source, String destination, String startTime, String selectedVehicle, String status)
    {
        this.startDate = startDate;
        this.endDate  =endDate;
        this.source = source;
        this.destination = destination;
        this.startTime = startTime;
        this.selectedVehicle = selectedVehicle;
        this.status = status;
    }
}
