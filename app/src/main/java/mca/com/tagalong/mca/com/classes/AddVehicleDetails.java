package mca.com.tagalong.mca.com.classes;

public class AddVehicleDetails {

    public String getVehicleRegistrationNumber() {
        return vehicleRegistrationNumber;
    }

    public void setVehicleRegistrationNumber(String vehicleRegistrationNumber) {
        this.vehicleRegistrationNumber = vehicleRegistrationNumber;
    }

    public String getVehicleDesc() {
        return vehicleDesc;
    }

    public void setVehicleDesc(String vehicleDesc) {
        this.vehicleDesc = vehicleDesc;
    }

    public Boolean getPillionHelmet() {
        return pillionHelmet;
    }

    public void setPillionHelmet(Boolean pillionHelmet) {
        this.pillionHelmet = pillionHelmet;
    }

    public String getVehicleDLNumber() {
        return vehicleDLNumber;
    }

    public void setVehicleDLNumber(String vehicleDLNumber) {
        this.vehicleDLNumber = vehicleDLNumber;
    }

    public String getVehicleClass() {
        return vehicleClass;
    }

    public void setVehicleClass(String vehicleClass) {
        this.vehicleClass = vehicleClass;
    }

    private String vehicleRegistrationNumber;
    private String vehicleDesc;
    private Boolean pillionHelmet;
    private String vehicleDLNumber;
    private String vehicleClass;

    public AddVehicleDetails() {
    }

    public AddVehicleDetails(String vehicleRegistrationNumber, String vehicleDesc, String vehicleClass,
                             Boolean pillionHelmet, String vehicleDLNumber)
    {
        this.vehicleRegistrationNumber = vehicleRegistrationNumber;
        this.vehicleDesc = vehicleDesc;
        this.vehicleClass = vehicleClass;
        this.pillionHelmet = pillionHelmet;
        this.vehicleDLNumber = vehicleDLNumber;
    }

    public AddVehicleDetails(String vehicleRegistrationNumber, String vehicleDesc, String vehicleClass,
                             Boolean pillionHelmet)
    {
        this.vehicleRegistrationNumber = vehicleRegistrationNumber;
        this.vehicleDesc = vehicleDesc;
        this.vehicleClass = vehicleClass;
        this.pillionHelmet = pillionHelmet;
    }

    @Override
    public String toString(){
       return this.vehicleRegistrationNumber + this.vehicleDesc
                            + this.vehicleClass + this.pillionHelmet + this.vehicleDLNumber;
    }
}
