package mca.com.tagalong.mca.com.classes;

public class AddPassenger {
    private String passengerFirstName;
    private String passengerLastName;
    private String contactNumber;
    private String passengerDOB;
    private String passengerGender;

    public AddPassenger(){
    }

    public AddPassenger(String passengerFirstName, String passengerLastName, String contactNumber, String passengerDOB, String passengerGender){
        this.passengerFirstName = passengerFirstName;
        this.passengerLastName = passengerLastName;
        this.contactNumber  =contactNumber;
        this.passengerDOB = passengerDOB;
        this.passengerGender = passengerGender;
    }


    public String getPassengerFirstName() {
        return passengerFirstName;
    }

    public void setPassengerFirstName(String passengerFirstName) {
        this.passengerFirstName = passengerFirstName;
    }

    public String getPassengerLastName() {
        return passengerLastName;
    }

    public void setPassengerLastName(String passengerLastName) {
        this.passengerLastName = passengerLastName;
    }

    public String getPassengerDOB() {
        return passengerDOB;
    }

    public void setPassengerDOB(String passengerDOB) {
        this.passengerDOB = passengerDOB;
    }

    public String getPassengerGender() {
        return passengerGender;
    }

    public void setPassengerGender(String passengerGender) {
        this.passengerGender = passengerGender;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
}
