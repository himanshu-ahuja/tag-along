package mca.com.tagalong.mca.com.verification;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import mca.com.tagalong.R;

public class AadharList {

    private Map<String, String> aadharMap = new HashMap<>();
    String aadharList[] = new String[50];
    private static final String TAG = "AADHAR";
    Boolean valid;

    public boolean isAadharValid(Context context, String aadhar) {
        BufferedReader bufferedReader;
        //run code once
        try {
            InputStream inputStream = context.getResources().openRawResource(R.raw.aadhar_phone);

            String line;
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = bufferedReader.readLine()) != null) {
                String[] parts = line.split(" ", 2);
                if (parts.length >= 2) {
                    String key = parts[0];
                    String value = parts[1];
                    aadharMap.put(key, value);
                } else {
                    Log.d(TAG, "Ignoring line: " + line);
                }
            }
            bufferedReader.close();
            Log.d(TAG, String.valueOf(aadharMap.size()));
            valid = aadharMap.containsKey(aadhar);
            return valid;
        } catch (IOException e) {
            Log.d(TAG, "F.E!");
        }
       return  false;
    }

    public String getAadharValue(String key){
        return aadharMap.get(key);
    }

}
