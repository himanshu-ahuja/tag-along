package mca.com.tagalong.mca.com.verification;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import mca.com.tagalong.R;

public class DrivingLicenseList {
    private static final String TAG = "DL";
    private List<String> licenseList = new ArrayList<>();

    public boolean isDrivingLicenseValid(Context context, String number) throws IOException {
        final  InputStream inputStream = context.getResources().openRawResource(R.raw.driving_license);
        final Scanner scanner = new Scanner(inputStream);
        try{
            while (scanner.hasNext()){
                licenseList.add(scanner.next());
            }
            Log.d(TAG, String.valueOf(licenseList.size()));
        }
        finally {
            scanner.close();
            inputStream.close();
        }
        return licenseList.contains(number);
    }
}
