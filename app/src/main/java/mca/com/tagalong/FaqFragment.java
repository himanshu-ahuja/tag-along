package mca.com.tagalong;


import android.app.Fragment;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FaqFragment extends Fragment {


    public FaqFragment() {
        // Required empty public constructor
    }

    Button q1,q2,q3,q4;
    TextView ans1,ans2,ans3,ans4;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_faq, container, false);

        q1=(Button)view.findViewById(R.id.Abouttagalong);
        q2=(Button)view.findViewById(R.id.Usetagalong);
        q3=(Button)view.findViewById(R.id.Securetagalong);
        q4=(Button)view.findViewById(R.id.Paytagalong);

        ans1=(TextView) view.findViewById(R.id.AbouttagalongAns);
        ans2=(TextView)view.findViewById(R.id.UsetagalongAns);
        ans3=(TextView)view.findViewById(R.id.SecuretagalongAns);
        ans4=(TextView)view.findViewById(R.id.PaytagalongAns);


        final ViewGroup questions =(ViewGroup)view.findViewById(R.id.Questions);


        q1.setOnClickListener(new View.OnClickListener() {

            boolean visible = false;
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition(questions);
                visible=true;
                ans1.setVisibility(View.VISIBLE);

            }
        });

        q2.setOnClickListener(new View.OnClickListener() {

            boolean visible = false;
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition(questions);
                visible=true;
                ans2.setVisibility(View.VISIBLE);

            }
        });
        q3.setOnClickListener(new View.OnClickListener() {

            boolean visible = false;
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition(questions);
                visible=true;
                ans3.setVisibility(View.VISIBLE);

            }
        });
        q4.setOnClickListener(new View.OnClickListener() {

            boolean visible = false;
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition(questions);
                visible=true;
                ans4.setVisibility(View.VISIBLE);

            }
        });
        return view;
    }

}
